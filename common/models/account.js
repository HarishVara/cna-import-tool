module.exports = function(Account) {
       Account.status = function(id,cb){
        Account.find(function(err,instance){
            //response = "Email is: "+instance.email;
            console.log(instance);
            response = instance;
            cb(null,response);
            console.log(response);
        });
    }
    
    Account.remoteMethod(
        'status',
        {
            http:{path:'/status',verb:'get'},
            accepts: {arg: 'id', type: 'string', http: { source: 'query' } },
            returns:{arg:'status',type:'string'}
        }
    );
};
